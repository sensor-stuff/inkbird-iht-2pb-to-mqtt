#include <WiFi.h>
#include <BLEDevice.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>
#include "settings.h"

// https://github.com/256dpi/arduino-mqtt
// 2.5.1
#include <MQTTClient.h>

// https://arduinojson.org/
// 6.21.2
#include <ArduinoJson.h>

typedef struct{
    bool enabled;
    char* id;
    unsigned long lastSeen;
    bool logical;    
    float temperature;
    void data(uint8_t *);
    bool isActive();
    bool isActive(uint16_t);
    double latestValue();


} Probe;

void Probe::data(uint8_t *pData){

    if(!enabled){
        return;
    }

    lastSeen = millis();

    if(pData[4] >= 254){
        // negative
        temperature = (float) ((255 * (pData[4] - 255)) + (pData[5]-255)) / 10;
    }else if(pData[4] <= 11){
        //positive
        temperature = (float) ((255 * pData[4]) + pData[5]) / 10;
    }else{
        // out of bounds
        logical = false;
        return;
    }

    logical = (temperature >= -50 && temperature <= 300);

    //DEBUG_PRINT(pData[4]); DEBUG_PRINT(".");DEBUG_PRINTLN(pData[5]);
}

bool Probe::isActive(uint16_t threshold){
    if(!enabled){
        return false;
    }
    if(!lastSeen){
        return false;
    }

    return (millis() - lastSeen < threshold);
}

double Probe::latestValue(){

    if(!enabled){
        return 999;
    }
    if(!logical){
        return 998;
    }

    return (int)(temperature * 100 + 0.5) / 100.0;
}

typedef struct{
    volatile bool enabled;
    uint32_t interval;
    volatile uint32_t lastRun;
    volatile bool state;
    void check();
    bool run();

} Timer;

void ICACHE_RAM_ATTR Timer::check(){
    if(!enabled){
        return;
    }
    if(millis() - lastRun >= interval){
        state = true;
        lastRun = millis();
    }
}

bool Timer::run(){
    if(!enabled){
        return false;
    }

    if(state){
        state = false;
        return true;
    }
    return false;
}


bool foundInkbird = false, haveWifi = false;
volatile bool breakDelay = false;

hw_timer_t *cronTimer = nullptr;

uint8_t lastWifiEvent = 0;
//char lastWill[50];
//char reportTopic[50];

char* lastWill = new char[50];
char* reportTopic = new char[50];

const uint8_t value1[] = {0x55, 0xAA, 0x1A, 0x01, 0x00, 0x1A};
const uint8_t value2[] = {0x55, 0xAA, 0x19, 0x01, 0x00, 0x19};

const BLEUUID serviceUUID("0000ffe0-0000-1000-8000-00805f9b34fb");

const BLEUUID characteristic1("0000ffe4-0000-1000-8000-00805f9b34fb");
const BLEUUID characteristic2("0000ffe9-0000-1000-8000-00805f9b34fb");


Timer timers[2];

Probe probes[4];

BLEDevice *pDevice;
BLEClient *pClient;
BLEScan *pBLEScan;
BLEAddress *pServerAddress;
BLEAddress *targetAddr;

BLERemoteCharacteristic *pCharacteristic1, *pCharacteristic2;

WiFiClient net;
MQTTClient mqtt(256);

